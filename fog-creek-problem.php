<?php

//Reads string from file and returns an array with characters as keys and frequency as values
function strToArray($file){
    if ($handle = fopen($file,'r')){
        $string = fread($handle,filesize($file)); //filesize function returns number of bytes in file
        fclose($handle);
    }
    $strArray = str_split(preg_replace('/\s+/', '', $string)); //preg_replace gets rid of all whitespaces; str_split converts string to array
    $arrLen = array_count_values($strArray); //will return a new array with the number of each value in old array
    return $arrLen;
}

//I took the harder route with this function. It passes new array into a workable array in sorting mechanism, and then passes it back into a readable array
function stringSorter($newArray){
    $test = array();
    foreach($newArray as $v){
        $test[] = $v;
    }

    for ($i = 1; $i < count($test); $i++){
        $key = $test[$i]; //gets the key (value to be compared with existing values)
        $j = $i-1; //gets the array index for comparison
        while ($j>=0 && $test[$j]<$key){
            $test[$j+1] = $test[$j];
            $j = $j-1;
            $test[$j+1] = $key;
        }
    }
    $result = array();
    foreach($test as $k=>$v){
        $keys_array = array_keys($newArray, $v); //returns keys and values from previous original array
        foreach($keys_array as $majorKey){
            $result[$majorKey] = $v;
        }
    }
    return $result;
}

//Final step to switch keys and values in array received from last function, convert to string, and parse final word
function arrToString($array){
    $flip = array_flip($array);
    $str = implode("",$flip);
    $arrayAfter = explode('_',$str);
    return $arrayAfter[0];
}


//Input
$arrayWithVal=strToArray("filetest.txt"); //call on function to open and process file
$newArray = $arrayWithVal;
$pass = stringSorter($newArray);
$stuff = arrToString($pass);
print_r($stuff);
?>